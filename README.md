# Classification task as per NDL Interview 2016

## Usage

### By Makefile

```
// install dependencies
make venv
// classify question categories
make classify_categories
// predict answer difficulty
make predict_answer
```

### Manual

Install the dependencies by running `pip install -r requirements.txt` and then run the following command :

```
python classifier.py -c --clusters [num of clusters] (required)
					-l --load_categories (load pretrained categories)
					-d --drop_rounds (drop questions other than Jeopardy & Double Jeopardy rounds)
					-w --word2vec (tokenize the sentence set with word2vec)
					-t --load_tokens (load precomputed tokens for categories)
					-a --load_answers (load precomputed tokens for answers)
					-r --run_answers (run prediction on answer difficulty)
```

## Explanation

In my analysis, I had first taken the task to classify question category. But the dataset contains about 28 thousand different question categories, so naturally the classifier is unable to perform a proper classification. In order to reduce this category, we need to group questions which are semantically similar, i.e having a broader sub-group. Now manually grouping this large number of categories is also a futile attempt, thus I used K-Means clustering to cluster the categories.

The classifier itself cannot understand text data, so we can use tf-idf to vectorize the categories, but since we are looking for semantically similar categories I used a different approach : Using a [pretrained word2vec model](http://nlp.stanford.edu/data/glove.6B.zip)[1], trained on [Wikipedia 2014](http://dumps.wikimedia.org/enwiki/20140102/) corpus, to generate vectors for each category, which is then used in K-Means to find clusters. Using Elbow method, where I measured the training cost each time we make a cluster and take the lowest value. Here, I have chosen 10 clusters based on findings, which are formed of semantically similar categories, such as Movies and Songs in one group (“American Movies, April Songs, B Actors, B Movies etc), “A” in X category (A in Geography, A in History, A in Science etc) and so on. Thus the target class is significantly reduced to a more manageable level.

Next, the questions and  answers texts are clubbed together in a single string, converted to a list of words with stop words dropped and vectorized by tf-idf, and then run through a Logistic Regression classifier to classify the target class category. Still the classifier can achieve only an overall accuracy of 32%, which can be explained to the fact since a question text, even when vectorized, has lots of common words throughout the categories.

One of the takeaways of this approach is the clustering of categories based on the wikipedia corpus. While there are some groups which the categories don't have any apparent similarity, majority of the clusters have semantic similarity. For example, closely knit groups are Movies, A in X category, and History & Literature (“...od words”, literature, ancient greeks, adjectives), which contribute more towards the end classification. Given a new question, probability of it being a Movie centric question is thus higher than Science related questions, with more than 12% of total share.

So, I looked at another problem, what are the factors for difficulty of a question. If a classifier effectively approximates the vector of a particular answer from various independent variables in the dataset, that would give us an insight into which are the key factors for a difficult question, as it calculates how hard is to locate the answer in vector space given the independent variables. Since we have a word2vec model from Wikipedia 2014 corpus, we can get the measure of how “common” a particular answer is from it. The reason of this is that rare events might not be widely registered in the wikipedia corpus.

The independent variables in scrutiny are :

* Question round, where question becomes difficult from “Jeopardy” to “Double Jeopardy” and even more in “Final Jeopardy”
* Value of the question in dollars, as mostly difficulty directly correlates with the value amount, previously clustered category information, and
* a small heuristic of length of a particular question, as difficult questions have a bit more explanation to confuse the participants.

Therefore, using a binary categorized representation of the word2vec vector of answers as target, a Random Forest Classifier with 10 estimators, gives an accuracy of 78%, which says that the category, value, round and question length offer enough information about the vectoral position of the answer, hence are the key factors in determining the difficulty of the question.

[1] Pretrained in [Glove](http://nlp.stanford.edu/projects/glove/), [converted](https://github.com/manasRK/glove-gensim) to be compatible in word2vec