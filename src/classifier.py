import pandas as pd
import numpy as np
import re
from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import train_test_split
import nltk
from nltk.corpus import stopwords
from gensim.models import word2vec
from sklearn.cluster import MiniBatchKMeans
from sklearn.preprocessing import Imputer
from sklearn.preprocessing import normalize
from sklearn.metrics import classification_report
import matplotlib.pyplot as plt
import cPickle as pickle
import argparse

import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s',\
    level=logging.INFO)

import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

CATFILE = 'data/catVecs.pkl'
DATAFILE = 'data/data.pkl'
GLOVEFILE = 'data/glove.6B.50d.w.txt'
TRAINTOKS = 'data/traintoks.pkl'
TESTTOKS = 'data/testtoks.pkl'
TRAINTOKS_W = 'data/traintoks_w.pkl'
TESTTOKS_W = 'data/testtoks_w.pkl'
ANSWERVECS = 'data/answers.pkl'
glove = {}
glove_loaded = False

def stripUrl(url):
	""" strip URL's from text
	"""
	return re.sub('<[^<]+?>','',url)

def process(dropRounds=False):
	""" initial data preprocessing and cleaning.
	"""
	print "Starting processing of raw data ..."
	clean_data = pd.read_json('JEOPARDY_QUESTIONS.json')
	clean_data['value'] = clean_data['value'].str.replace('$','')
	clean_data['value'] = pd.to_numeric(clean_data['value'],errors='coerce')
	clean_data['qna'] = clean_data['question'] + ' ' + clean_data['answer']
	clean_data['qnac'] = clean_data['qna'] + ' ' + clean_data['category']

	clean_data.value.fillna(0, inplace=True)
	clean_data = clean_data[pd.notnull(clean_data['qnac'])]
	clean_data['sqna'] = clean_data.qna.apply(lambda x : stripUrl(x))
	clean_data['sqnac'] = clean_data.qnac.apply(lambda x : stripUrl(x))

	clean_data['categoryN'] = pd.Categorical(clean_data.category).codes
	clean_data['roundN'] = pd.Categorical(clean_data['round']).codes
	clean_data['aired'] = pd.to_datetime(clean_data['air_date'])
	clean_data['segVal'] = clean_data['value'].apply(lambda x : seggregate_difficulty(x))
	clean_data['qlen'] = clean_data.question.apply(lambda x : len(x))

	if dropRounds is True:
		clean_data = clean_data[clean_data['round'] != 'Final Jeopardy!']
		clean_data = clean_data[clean_data['round'] != 'Tiebreaker']

	clean_data = dropOutliers(clean_data)
	print "Processing data complete."
	return clean_data

def question_to_wordlist( question, remove_stopwords=False ):
	""" convert a sentence into an array of words, with an option to remove stopwords
	"""
	question_text = re.sub("[^a-zA-Z]"," ", question)
	words = question_text.lower().split()
	if remove_stopwords:
	    stops = set(stopwords.words("english"))
	    words = [w for w in words if not w in stops]
	return(words)

def tokenize(X_train, X_test,word2vecFlag=False):
	""" Tokenize the train and test sentences
	word2vec flag if set true will tokenize the sentences and generate
	vectors based on pretrained model. otherwise only break the sentences into words,
	dropping stopwords
	"""
	clean_train_questions = []
	for question in X_train:
		clean_train_questions.append( question_to_wordlist( question, remove_stopwords=True ))
	print "Tokenized %d train sentences" % len(clean_train_questions)
	clean_test_questions = []
	for question in X_test:
		clean_test_questions.append( question_to_wordlist( question, remove_stopwords=True ))
	print "Tokenized %d test sentences" % len(clean_test_questions)
	if word2vecFlag == True:
		global glove_loaded
		global glove
		if glove_loaded == False:
			glove = word2vec.Word2Vec.load_word2vec_format(GLOVEFILE, binary=False)
			glove_loaded = True
		trainDataVecs = Imputer().fit_transform(getAvgFeatureVecs(clean_train_questions, glove, 50))
		testDataVecs = Imputer().fit_transform(getAvgFeatureVecs(clean_test_questions, glove, 50))
		return (trainDataVecs, testDataVecs)
	else:
		return (clean_train_questions, clean_test_questions)

def dropOutliers(clean_data):
	""" Drop unexpected value outliers which does not conform to Jeopardy game rules
	"""
	bad_values = [5,250,750,350,50,20,22,2547,367,601,796,585]
	mask = clean_data['value'].isin(bad_values)
	clean_data = clean_data[~mask]
	return clean_data

def seggregate_difficulty(val):
	""" Segregate the dollar value amount into 4 different levels
	"""
	if val >= 1200:
	    return 4
	if val >= 700:
	    return 3
	if val >= 300:
	    return 2
	else:
	    return 1

def makeFeatureVec(words, model, num_features, index2word_set):
	""" Function to average all of the word vectors in a given paragraph """
	featureVec = np.zeros((num_features,),dtype="float32")
	nwords = 0.
	for word in words:
	    if word in index2word_set:
	        nwords = nwords + 1.
	        featureVec = np.add(featureVec,model[word])
	featureVec = np.divide(featureVec,nwords)
	return featureVec


def getAvgFeatureVecs(questions, model, num_features):
	""" Given a set of reviews (each one a list of words), calculate
	the average feature vector for each one and return a 2D numpy array
	"""
	counter = 0.
	qFeatureVecs = np.zeros((len(questions),num_features),dtype="float32")
	index2word_set = set(model.index2word)
	for question in questions:
	   if counter%1000. == 0.:
	       print "Question %d of %d" % (counter, len(questions))
	   qFeatureVecs[counter] = makeFeatureVec(question, model, \
	       num_features, index2word_set)
	   counter = counter + 1.
	return qFeatureVecs

def categoryToVectors(df, num_features, cache=False):
	""" Generate an unique list of categories and create a vector representation
	with the help of a pretrained word2vec model
	"""
	if cache == True:
		print "Loading pretrained vectors"
		catVecs = pickle.load(open(CATFILE,'r'))
	else:
		## Load Glove
		global glove_loaded
		global glove
		print "Loading pre-trained glove-gensim model"
		if glove_loaded == False:
			glove = word2vec.Word2Vec.load_word2vec_format(GLOVEFILE, binary=False)
			glove_loaded = True
		print "Loading done. Proceeding vector generation ..."
		categories = []
		uniqCats = np.unique(df.category)
		print "%d unique categories found..." % len(uniqCats)
		for category in uniqCats:
			categories.append( question_to_wordlist( category, remove_stopwords=True ))
		print categories[0:5]
		catVecs = getAvgFeatureVecs( categories, glove, num_features )
		if np.isnan(catVecs).any() == True:
			catVecs[np.isnan(catVecs)] = 0
		print "Vectors generated"
		print "Saving vectors for future use ..."
		pickle.dump(catVecs, open(CATFILE,'wb'))
	return catVecs

def answerToVectors(df, num_features, cache=False):
    """ Generate a vector representation of answers
    with the help of a pretrained word2vec model
    """
    if cache == True:
        print "Loading pretrained vectors"
        ansVecs = pickle.load(open(ANSWERVECS,'r'))
    else:
        ## Load Glove
				global glove_loaded
				global glove
				if glove_loaded == False:
					glove = word2vec.Word2Vec.load_word2vec_format(GLOVEFILE, binary=False)
					glove_loaded = True
				print "Loading done. Proceeding vector generation ..."
				answers = []
				for answer in df.answer:
				    answers.append( question_to_wordlist( answer, remove_stopwords=True ))
				ansVecs = getAvgFeatureVecs( answers, glove, num_features )
				if np.isnan(ansVecs).any() == True:
				    ansVecs[np.isnan(ansVecs)] = 0
				print "Vectors generated"
				print "Saving vectors for future use ..."
				pickle.dump(ansVecs, open(ANSWERVECS,'wb'))
    return ansVecs

def clusterCategories(catVecs, num_clusters):
	print "Clustering categories"
	mk = MiniBatchKMeans(n_clusters=num_clusters)
	mk.fit(catVecs)
	print "Clustering done"
	print "Cost  : %f" % mk.inertia_
	return (mk.labels_,mk.inertia_)

def applyCategories(df, catIds, colname):
	cat2ids = {}
	cats = np.unique(df.category)
	for idx,cat in enumerate(cats):
		cat2ids[cat] = catIds[idx]
	df[colname] = df.category.apply(lambda x : cat2ids[x])
	return df

def displayCluster(df, num_clusters):
	""" Display 5 rows from each cluster """
	for c in range(num_clusters):
		grouped = df[df.cat2id == c]
		for cat in np.unique(grouped.category)[0:5]:
			print "%s, " % cat
		print
		print "Number of records : %d" % len(grouped)

def categorizeAnswers(row,n):
	""" Categorize answer vectors, each element being transformed into
			n sized one hot vectors
	"""
	gap = (np.max(row) - np.min(row)) / (n - 1)
	segvec = np.array([])
	for i,elem in enumerate(row):
	  s = np.zeros(n)
	  ind = np.floor((elem - np.min(row)) / gap)
	  if ind > -1:
	      s[ind] = 1
	  segvec = np.concatenate((segvec,s))
	return segvec

def trans(x):
	return np.array(x).reshape(-1,1)

def runPipeline(data,pipes, args):
	print "Splitting train test data"
	X_train, X_test, y_train, y_test = train_test_split(data[['sqna','value','roundN','qlen']], data.cat2id)
	print "Tokenizing ..."
	if args.load_tokens:
		if args.word2vec:
			X_train = pickle.load(open(TRAINTOKS_W,'r'))
			X_test = pickle.load(open(TESTTOKS_W, 'r'))
		else:
			X_train = pickle.load(open(TRAINTOKS,'r'))
			X_test = pickle.load(open(TESTTOKS, 'r'))
		print "Loaded tokens"
	else:
		X_train_t, X_test_t = tokenize(X_train.sqna, X_test.sqna, args.word2vec)
		if args.word2vec:
			X_train = np.hstack((X_train_t,trans(X_train.value),trans(X_train.roundN),trans(X_train.qlen)))
			X_test = np.hstack((X_test_t,trans(X_test.value),trans(X_test.roundN),trans(X_test.qlen)))
			pickle.dump(X_train,open(TRAINTOKS_W, 'wb'))
			pickle.dump(X_test,open(TESTTOKS_W, 'wb'))
		else:
			X_train = X_train_t
			X_test = X_test_t
			pickle.dump(X_train,open(TRAINTOKS, 'wb'))
			pickle.dump(X_test,open(TESTTOKS, 'wb'))
	accuracies = []
	model = {}
	for method in pipes:
		if method == 'log_tfidf':
			if args.word2vec:
				model = Pipeline([
					("logistic_regression", LogisticRegression(solver='lbfgs',multi_class='ovr',n_jobs=-1))
				])
			else:
				model = Pipeline([("tfidf_vectorizer", TfidfVectorizer(analyzer=lambda x: x)),
					("logistic_regression", LogisticRegression(solver='lbfgs',multi_class='ovr',n_jobs=-1))
				])
		if method == 'random_forest':
			model = Pipeline([("tfidf_vectorizer", TfidfVectorizer(analyzer=lambda x: x)),
				("random_forest", RandomForestClassifier(max_features='sqrt',n_estimators = 100, n_jobs = -1, verbose = 1 ))
			])
		if method == 'linear_svc':
			model = Pipeline([("tfidf_vectorizer", TfidfVectorizer(analyzer=lambda x: x)),
				("linear svc", SVC(kernel="linear"))
			])
		print "Fitting model %s..." % method
		model.fit(X_train,y_train)
		print "Model fitting done."
		print "Starting prediction ..."
		y_pred = model.predict(X_test)
		print "Prediction for %s model complete." % method
		accuracies.append(np.mean(y_pred == y_test))
		print(classification_report(y_test, y_pred))
		pickle.dump(data,open(DATAFILE, 'wb'))
	return accuracies

def classifyAnswerDifficulty(data, args):
	print "Generating answer vectors"
	ansVecs = answerToVectors(data, 50, args.load_answers)
	print "Categorizing answers"
	ansVecBin = map(lambda x : categorizeAnswers(x,5),ansVecs)
	ansVecBin = np.array(ansVecBin)
	X = data[['value','roundN','cat2id','qlen']]
	print "Splitting train and test"
	msk = np.random.rand(len(X)) < 0.8
	X_train = X[msk]
	X_test = X[~msk]
	y_train = ansVecBin[msk]
	y_test = ansVecBin[~msk]
	print "Cleaning up RAM before running classification"
	glove = {}
	data = {}
	ansVecBin = {}
	clf = RandomForestClassifier(n_estimators = 10, n_jobs = -1, verbose = 1 )
	print "Fitting random_forest"
	clf.fit(X_train,y_train)
	print "Fitting done"
	print "Predicting ..."
	yp = clf.predict(X_test)
	importances = clf.feature_importances_
	acc = np.mean(yp==y_test)
	print "Classifier accuracy : %f" % acc
	importances = clf.feature_importances_
	std = np.std([tree.feature_importances_ for tree in clf.estimators_],
	             axis=0)
	indices = np.argsort(importances)[::-1]
	names = X.columns.values[indices]

	# Print the feature ranking
	print("Feature ranking:")

	for f in range(X.shape[1]):
	    print("%d. feature %d (%f)" % (f + 1, indices[f], importances[indices[f]]))

	# Plot the feature importances of the forest
	plt.figure()
	plt.title("Feature importances")
	plt.bar(range(X.shape[1]), importances[indices],
	       color="r", yerr=std[indices], align="center")
	plt.xticks(range(X.shape[1]), names)
	plt.xlim([-1, X.shape[1]])
	plt.show()

def checkLowestCluster(data, catVecs, searchLimit=10):
	""" Helper function to generate minimum cluster size from cost
	"""
	min_clusters = 2
	min_cost = 9999999
	for i in range(2,searchLimit):
		print "Iteration %d" % i
		catIds, cost = clusterCategories(catVecs,i)
		if cost < min_cost:
			cost = min_cost
			min_clusters = i

	return min_clusters

def runClassification(args):
	pipes = ['log_tfidf']
	data = process(args.drop_rounds)
	catVecs = categoryToVectors(data, 50, args.load_categories)
	catIds,cost = clusterCategories(catVecs,args.clusters)
	data = applyCategories(data, catIds, 'cat2id')
	print "Displaying categories"
	displayCluster(data,args.clusters)
	if args.run_answers:
		classifyAnswerDifficulty(data, args)
	else:
		acc = runPipeline(data, pipes, args)
		print "Classification accuracy :"
		for i,a in enumerate(acc):
			print "%s : %f" % (pipes[i], a)

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('-c','--clusters',type=int, help='Number of Clusters', required=True)
	parser.add_argument('-l','--load_categories', help='Load pretrained category vector',action="store_true")
	parser.add_argument('-d','--drop_rounds',help='Drop extra rounds',action="store_true")
	parser.add_argument('-w','--word2vec',help='Tokenize questions with word2vec',action="store_true")
	parser.add_argument('-t','--load_tokens',help='Load precomputed tokens',action="store_true")
	parser.add_argument('-a','--load_answers',help='Load precomputed tokens for answers',action="store_true")
	parser.add_argument('-r','--run_answers',help='Run answer difficulty prediction',action="store_true")
	args = parser.parse_args()

	#check nltk stopwords presence
	try:
		nltk.data.find('stopwords')
	except LookupError:
		nltk.download('stopwords')

	runClassification(args)
