# Makefile
venv: venv/bin/activate
venv/bin/activate: requirements.txt
	test -d venv || virtualenv venv -p /usr/bin/python2.7
	venv/bin/pip install -Ur requirements.txt
	touch venv/bin/activate

classify_categories: venv
	venv/bin/python src/classifier.py -c 10

predict_answer: venv
	venv/bin/python src/classifier.py -c 10 -r